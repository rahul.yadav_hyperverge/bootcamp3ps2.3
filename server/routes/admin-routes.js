const express = require('express');

const adminController = require('../controllers/admin-controller');
const checkAuth = require('../middleware/check-auth');

const router = express.Router();

// TODO: add auth check for admin
router.use(checkAuth);

router.post('/addMovie', adminController.addMovie);
// TODO: delete movie router
// router.delete('/resetBus/:busId', adminController.resetBus);

module.exports = router;
