const express = require('express');

const queryController = require('../controllers/query-controller');

const router = express.Router();

router.get('/getMovies', queryController.getMovies);
router.get('/ticketInfo/:ticketId', queryController.getTicketInfo);
router.get('/movie/:movieName', queryController.getMovieByName);

module.exports = router;
