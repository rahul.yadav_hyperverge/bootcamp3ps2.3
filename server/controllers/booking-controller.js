const HttpError = require('../models/http-error');
const {
    findTheaterById,
    createNewTicket,
    createNewUser,
} = require('../utils/db-utils');

const bookTicket = async (req, res, next) => {
    let ticket;
    try {
        const { theaterId, movieId, userDetails, seatNumber } = req.body;
        const theater = await findTheaterById(theaterId);
        console.log(theater);
        const user = await createNewUser({
            name: userDetails.name,
            phoneNumber: userDetails.phoneNumber,
        });
        ticket = await createNewTicket({
            theaterId,
            movieId,
            seatNumber,
            user: user._id,
        });
        theater.tickets.push(ticket);
        await theater.save();
    } catch (err) {
        if (err.code) {
            return next(err);
        }
        return next(new HttpError('Booking failed, please try again', 500));
    }
    res.status(200).json({ booked: true, ticketId: ticket._id });
};

exports.bookTicket = bookTicket;
