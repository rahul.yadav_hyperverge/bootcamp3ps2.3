const HttpError = require('../models/http-error');
const { createNewMovie, createNewTheater } = require('../utils/db-utils');

const addMovie = async (req, res, next) => {
    const data = req.body;
    let movie;
    try {
        const theater = await createNewTheater({
            location: data.location,
            price: data.price,
        });
        await theater.save();
        movie = await createNewMovie({
            name: data.name,
            description: data.description,
            duration: data.duration,
            theater: theater,
        });
    } catch (err) {
        const error = new HttpError(
            'New Movie creation failed, please try again later.',
            500
        );
        return next(error);
    }
    res.status(200).json({ message: "successfully movie added" });
};
// TODO: delete the movie
// const resetBus = async (req, res, next) => {
//     const busId = req.params.busId;
//     try {
//         await removeTicketsFromBus(busId);
//     } catch (err) {
//         if (err.code === 404) {
//             return next(err);
//         }
//         const error = new HttpError(
//             `Bus and it's ticket removal failed, please try again later.`,
//             500
//         );
//         return next(error);
//     }
//     res.status(200).json({ removed: true });
// };

exports.addMovie = addMovie;
// exports.resetBus = resetBus;
