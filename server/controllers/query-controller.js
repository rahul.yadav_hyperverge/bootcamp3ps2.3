const HttpError = require('../models/http-error');
const {
    findTicketById,
    findMovies,
    findMovieByName,
} = require('../utils/db-utils');

const getTicketInfo = async (req, res, next) => {
    const ticketId = req.params.ticketId;
    let ticket;
    try {
        ticket = await findTicketById(ticketId);
    } catch (err) {
        return next(err);
    }
    res.status(200).json({ ticket: ticket });
};

const getMovies = async (req, res, next) => {
    let movies;
    try {
        movies = await findMovies();
    } catch (err) {
        if (err.code) {
            return next(err);
        }
        return next(new HttpError('Cannot find movies ', 500));
    }
    res.status(200).json({ movies: movies });
};

const getMovieByName = async (req, res, next) => {
    console.log(req.params);
    const movieName = req.params.movieName;
    let movie;
    try {
        movie = await findMovieByName(movieName);
    } catch (err) {
        if (err.code) {
            return next(err);
        }
        return next(new HttpError('Cannot find bus for provided id.', 500));
    }
    res.status(200).json({ movie: movie });
};

exports.getMovies = getMovies;
exports.getMovieByName = getMovieByName;
exports.getTicketInfo = getTicketInfo;
