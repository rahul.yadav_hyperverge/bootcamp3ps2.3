const Admin = require('../models/admin');
const Movie = require('../models/movie');
const Ticket = require('../models/ticket');
const Theater = require('../models/theater');
const User = require('../models/user');

const HttpError = require('../models/http-error');

//ADMIN UTILS
const createNewAdmin = (email, password) => {
    let admin;
    admin = new Admin({
        email,
        password,
    });
    return admin;
};

const findAdminById = async (adminId) => {
    let admin;
    admin = await Admin.findById(adminId);
    if (!admin) {
        const error = new HttpError(
            'Could not find admin for the provided id.',
            404
        );
        throw error;
    }
    return admin;
};

const findAdminByEmail = async (adminMail) => {
    const admin = await Admin.findOne({ email: adminMail });
    return admin;
};

//MOVIE UTILS
const createNewMovie = async ({ name, description, duration, theater }) => {
    const hyphen= name.replace(/-/g, ' ');
    const movieName = hyphen.replace(/\w\S*/g, (w) => (w.replace(/^\w/, (c) => c.toUpperCase())));
    const movie = await Movie.findOne({ name: movieName });
    if (!movie) {
        //create new movie
        const newMovie = new Movie({
            name: movieName,
            description: description,
            duration: duration,
            theater: [theater],
        });
        await newMovie.save();
        return newMovie;
    } else {
        //add theater to the movie
        const updatedMovie = await movie.theater.push(theater);
        await movie.save();
        return movie;
    }
};

const findMovies = async () => {
    const movies = await Movie.find().select(['-theater']);
    if (!movies) {
        throw new HttpError('Could not find the buses', 404);
    }
    return movies;
};
const findMovieByName = async (movieName) => {
    const hyper = movieName.replace(/-/g, ' ');
    const name = hyper.replace(/\w\S*/g, (w) => (w.replace(/^\w/, (c) => c.toUpperCase())));
    const movie = await Movie.findOne({ name: name })
        .populate({
            path: 'theater',
            populate: {
                path: 'tickets',
            },
        })
        .exec();
    if (!movie) {
        throw new HttpError(
            'Could not find the movie for the provided id.',
            404
        );
    }
    return movie;
};

//TICKET UTILS
const createNewTicket = async ({ movieId, theaterId, seatNumber, user }) => {
    const ticket = new Ticket({
        movie: movieId,
        theater: theaterId,
        seatNumber: seatNumber,
        user: user,
    });
    await ticket.save();
    return ticket;
};
const findTicketById = async (ticketId) => {
    const ticket = await Ticket.findById(ticketId)
        .populate('movie')
        .populate('theater')
        .populate('user')
        .exec();
    if (!ticket) {
        throw new HttpError('Ticket Not Found', 404);
    }
    return ticket;
};

//THEATER UTILS
const createNewTheater = async ({ location, price }) => {
    const theater = new Theater({
        location: location,
        price: price,
    });
    return theater;
};
const findTheaterById = async (theaterId) => {
    const theater = await Theater.findById(theaterId);
    if (!theater) {
        throw new HttpError('Theater Not Found', 404);
    }
    return theater;
};

//USER UTILS
const createNewUser = async ({ name, phoneNumber }) => {
    const user = new User({
        name: name,
        phoneNumber: phoneNumber,
    });
    await user.save();
    return user;
};

exports.createNewUser = createNewUser;

exports.createNewTicket = createNewTicket;
exports.findTicketById = findTicketById;

exports.createNewTheater = createNewTheater;
exports.findTheaterById = findTheaterById;

exports.findAdminById = findAdminById;
exports.createNewAdmin = createNewAdmin;
exports.findAdminByEmail = findAdminByEmail;

exports.findMovies = findMovies;
exports.findMovieByName = findMovieByName;
exports.createNewMovie = createNewMovie;
