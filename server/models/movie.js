const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const movieSchema = new Schema({
	name: { 
    	type: String, 
		required: true 
	},
	description: { 
		type: String, 
		required: true 
	},
	duration: { 
		type: String, 
		required: true 
	},
	theater: [
		{
			type: mongoose.Types.ObjectId,
			ref:'Theater'
		}
	]
});

module.exports = mongoose.model('Movie', movieSchema);
