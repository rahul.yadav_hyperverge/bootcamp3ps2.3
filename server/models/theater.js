const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const theaterSchema = new Schema({
	location: { 
		type: String, 
		required: true 
	},
	price: { 
		type: Number, 
		required: true 
	},
	tickets: [
		{
			type: mongoose.Types.ObjectId,
			ref:'Ticket'
		}
	]
});

module.exports = mongoose.model('Theater', theaterSchema);
