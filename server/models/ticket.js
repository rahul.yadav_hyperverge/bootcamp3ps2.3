const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const ticketSchema = new Schema({
	movie: {
		type: mongoose.Types.ObjectId,
		ref: 'Movie',
	},
	theater:{
		type:mongoose.Types.ObjectId,
		ref:'Theater'
	},
	seatNumber: { 
		type: String, 
		required: true 
	},
	user: { 
		type: mongoose.Types.ObjectId, 
		ref: 'User' 
	},
});

module.exports = mongoose.model('Ticket', ticketSchema);
