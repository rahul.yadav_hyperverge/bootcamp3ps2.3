import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import './App.css';
import { HomePage, BookingPage, TicketPage, AdminPage } from './index';
import useApp from './components/logic/useApp';

function App() {
    const {
        setTokenState,
        isAdmin
    } = useApp();
    return (
        <div className="app">
            <Router>
                <Switch>
                    <Route path="/" exact component={() => <HomePage />} />
                    <Route
                        path="/booking/:movieName"
                        exact
                        component={() => <BookingPage isAdmin={isAdmin} />}
                    />
                    <Route
                        path="/ticket/:ticketId"
                        exact
                        component={() => <TicketPage />}
                    />
                    <Route
                        path="/admin"
                        exact
                        component={() => <AdminPage setTokenState={setTokenState} isAdmin={isAdmin}/>}
                    />
                </Switch>
            </Router>
        </div>
    );
}

export default App;
