import React from 'react';
import styles from './home.module.css';
import { BsThreeDots } from 'react-icons/bs';
import {
    AiOutlineArrowLeft,
    AiOutlineArrowRight,
    AiOutlineSearch,
} from 'react-icons/ai';
import useHome from '../../logic/useHome';
function HomePage() {
    const { 
        setSearchMovie,
        handleSearchMovie,
        handleSearchTicket,
        setSearchTicket
    } = useHome();
    return (
        <div className={styles.content}>
            <div className={styles.background}>
                <div className={styles.content_box}>
                    <div className={styles.detail_box}>
                        <div className={styles.movie_options}>
                            <div className={styles.search_ticket_option}>
                                <div className={styles.search_movie}>
                                    <input
                                        className={styles.search_input}
                                        placeholder="Ticket Status"
                                        onChange={e => setSearchTicket(e.target.value)}
                                    />
                                    <AiOutlineSearch onClick={handleSearchTicket}/>
                                </div>
                            </div>
                            <div className={styles.search_movie_option}>
                                <div className={styles.search_movie}>
                                    <input
                                        className={styles.search_input}
                                        placeholder="Search"
                                        onChange={e => setSearchMovie(e.target.value)}
                                    />
                                    <AiOutlineSearch onClick={handleSearchMovie}/>
                                </div>
                                <div className={styles.more_options}>
                                    <BsThreeDots />
                                </div>
                            </div>
                        </div>
                        <div className={styles.movie_details}>
                            <div className={styles.movie_release}>
                                DECEMBER 19
                            </div>
                            <div className={styles.movie_name}>
                                <div className={styles.movie_name_fill}>
                                    THE RISE OF
                                </div>
                                <div className={styles.movie_name_outline}>
                                    SKYWALKER
                                </div>
                            </div>
                            <div className={styles.movie_details_options}>
                                <div className={styles.movie_buy_ticket}>
                                    BUY A TICKET
                                </div>
                                <div className={styles.movie_watch_trailer}>
                                    WATCH THE TRAILER
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {/* <div className={styles.left_arrow}>
                <AiOutlineArrowLeft />
            </div> */}
            <div className={styles.right_arrow}>
                <AiOutlineArrowRight />
            </div>
        </div>
    );
}
export default HomePage;
