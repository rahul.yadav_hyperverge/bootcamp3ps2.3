import styles from './booking.module.css';
import { SiImdb } from 'react-icons/si';
import { FiClock } from 'react-icons/fi';
import DateSection from '../../organisms/dateSection/dateSection';
import DropDown from '../../atoms/dropDown/dropDown';
import Input from '../../atoms/Input/input';
import Button from '../../atoms/button/button';
import { useParams } from 'react-router-dom';
import useBooking from '../../logic/useBooking';
import SeatingSection from '../../organisms/seatingSection/seatingSection';

function BookingPage() {
    const { movieName } = useParams();
    const {
        movie,
        setDate,
        fare,
        setTheater,
        theater,
        theaterData,
        handleCheckout,
        setSeat,
        seatStatus,
        handleName,
        handlePhoneNumber,
    } = useBooking(movieName);
    return (
        <div className={styles.content}>
            <div className={styles.content_box}>
                <div className={styles.movie_description}>
                    <div className={styles.movie_description_box}>
                        <div className={styles.movie_poster}></div>
                        <div className={styles.movie_details}>
                            <div className={styles.movie_name}>
                                {movie.name}
                            </div>
                            <div className={styles.movie_genre}>
                                Action • Adventure
                            </div>
                            <div className={styles.movie_items}>
                                <div className={styles.movie_imdb}>
                                    <SiImdb />
                                    8.6/9
                                </div>
                                <div className={styles.movie_duration}>
                                    <FiClock />
                                    {movie.duration}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className={styles.black_line}></div>
                <div className={styles.booking_details}>
                    <div className={styles.booking_items}>
                        <div className={styles.booking_date}>
                            <div className={styles.booking_heading}>DATE</div>
                            <DateSection setDate={setDate} />
                        </div>
                        <div className={styles.booking_theater}>
                            <div className={styles.booking_heading}>
                                THEATER
                            </div>
                            <DropDown
                                setTheater={setTheater}
                                value={theater}
                                text="Theater"
                                data={theaterData}
                            />
                        </div>
                        {/* TODO: time selection  */}
                        {/* <div className={styles.booking_time}>
                            <div className={styles.booking_heading}>TIME</div>
                            <DropDown setvalue={setTime} value={time} text="Time"/>
                        </div> */}
                    </div>
                </div>
                <div className={styles.movie_seats}>
                    <SeatingSection setSeat={setSeat} seatStatus={seatStatus} />
                </div>
                <div className={styles.booking_form}>
                    <div className={styles.form_heading}>Details</div>
                    <div className={styles.input_boxs}>
                        <Input
                            onChange={handleName}
                            placeholder="Enter name"
                            backgroundColor="rgb(17,17,17)"
                            color='white'
                        />
                        <Input
                            onChange={handlePhoneNumber}
                            placeholder="Enter PhoneNumber"
                            backgroundColor="rgb(17,17,17)"
                            color='white'
                        />
                    </div>
                    <div className={styles.checkout_section}>
                        <div className={styles.price}>
                            <div className={styles.total_price_text}>
                                TOTAL AMOUNT
                            </div>
                            <div className={styles.total_price}>₹ {fare}</div>
                        </div>
                        <div className={styles.checkout}>
                            <Button onClick={handleCheckout} text="CHECKOUT" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}
export default BookingPage;
