import styles from './ticket.module.css';
import { GoLocation } from 'react-icons/go';
import { MdDateRange } from 'react-icons/md';
import { BiTime } from 'react-icons/bi';
import { HiOutlineCurrencyRupee } from 'react-icons/hi';
import { FaBarcode } from 'react-icons/fa';

import { useParams } from 'react-router-dom';
import useTicket from '../../logic/useTicket';
function Ticket() {
    const { ticketId } = useParams();
    const {
        movieName,
        movieDuration,
        moviePrice,
        movieRow,
        movieSeat,
        movieLocation,
        movieUserName
    } = useTicket(ticketId);
    return (
        <div className={styles.content}>
            <div className={styles.ticket_box}>
                <div className={styles.hole_top_punch}></div>
                <div className={styles.hole_bottom_punch}></div>
                <div className={styles.ticket_image}>
                    <div className={styles.movie_name}>{movieName}</div>
                </div>
                <div className={styles.ticket_username}>
                    {movieUserName}
                </div>
                <div className={styles.theater_location}>
                    <div className={styles.align_box}>
                        <div className={styles.align_icon}>
                            <GoLocation />
                        </div>
                        <div className={styles.align_text}>
                            {movieLocation}
                        </div>
                    </div>
                </div>
                <div className={styles.movie_details}>
                    <div className={styles.movie_date}>
                        <div className={styles.align_icon}>
                            <MdDateRange />
                        </div>
                        <div className={styles.align_text}>
                            5 Feb
                        </div>
                    </div>
                    <div className={styles.movie_timing}>
                        <div className={styles.align_icon}>
                            <BiTime />
                        </div>
                        <div className={styles.align_text}>
                            {movieDuration}
                        </div>
                    </div>
                    <div className={styles.movie_price}>
                        <div className={styles.align_icon}>
                            <HiOutlineCurrencyRupee />
                        </div>
                        <div className={styles.align_text}>
                            {moviePrice}
                        </div>
                    </div>
                </div>
                <div className={styles.movie_seating}>
                    <div className={styles.movie_row_section}>
                        <div className={styles.movie_text}>ROW</div>
                        <div className={styles.movie_row}>{movieRow}</div>
                    </div>
                    <div className={styles.movie_seat_section}>
                        <div className={styles.movie_text}>SEAT</div>
                        <div className={styles.movie_seat}>{movieSeat}</div>
                    </div>
                </div>
                <div
                    style={{
                        height: '2px',
                        width: '100%',
                        backgroundColor: 'rgb(224,224,224)',
                    }}
                ></div>
                <div className={styles.barcode_image}>
                    <div className={styles.barcode_images}>
                        <FaBarcode />
                        <FaBarcode />
                        <FaBarcode />
                    </div>
                </div>
            </div>
        </div>
    );
}
export default Ticket;
