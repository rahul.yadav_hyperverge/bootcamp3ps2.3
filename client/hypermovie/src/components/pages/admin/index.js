import styles from './admin.module.css';
import Input from '../../atoms/Input/input';
import Button from '../../atoms/button/button';
import useAdmin from '../../logic/useAdmin';
function Admin({ setTokenState, isAdmin }) {
    const {
        HandleEmail,
        HandlePassword,
        HandleLogIn,
        HandleLogout,
        HandleMovieDescription,
        HandleMovieDuration,
        HandleMovieName,
        HandleMoviePrice,
        HandleTheaterLocation,
        HandleMovieSubmit
    } = useAdmin({ setTokenState });
    if (isAdmin) {
        return (
            <div className={styles.content}>
                <div className={styles.admin_logout}>
                    <Button
                        text="Log Out"
                        backgroundColor="red"
                        onClick={HandleLogout}
                    />
                </div>
                <div className={styles.admin_panel}>
                    <div className={styles.panel_header}>
                        <div className={styles.panel_text}>Add Movie</div>
                        <div className={styles.panel_btn}>
                            <Button text="Add" onClick={HandleMovieSubmit}/>
                        </div>
                    </div>
                    <div className={styles.panel_form}>
                        <div style={{ display: 'flex' }}>
                            <Input
                                placeholder="Movie Name"
                                backgroundColor="rgb(240, 240, 240)"
                                marginBottom="10px"
                                onChange={HandleMovieName}
                            />
                            <Input
                                placeholder="Duration"
                                backgroundColor="rgb(240, 240, 240)"
                                marginBottom="10px"
                                onChange={HandleMovieDuration}
                            />
                        </div>
                        <textarea
                            type="text"
                            className={styles.display_box_value}
                            placeholder="Description"
                            onChange={HandleMovieDescription}
                        />
                        <div style={{ display: 'flex' }}>
                            <Input
                                placeholder="Theater location"
                                backgroundColor="rgb(240, 240, 240)"
                                marginBottom="10px"
                                onChange={HandleTheaterLocation}
                            />
                            <Input
                                placeholder="Price"
                                backgroundColor="rgb(240, 240, 240)"
                                marginBottom="10px"
                                onChange={HandleMoviePrice}
                            />
                        </div>
                    </div>
                </div>
            </div>
        );
    } else {
        return (
            <div className={styles.admin_login}>
                <div className={styles.admin_login_box}>
                    <div className={styles.heading_text}>Log In</div>
                    <div className={styles.admin_form}>
                        <Input
                            placeholder="Email"
                            backgroundColor="rgb(235, 235, 235)"
                            marginBottom="10px"
                            onChange={HandleEmail}
                        />
                        <Input
                            placeholder="Password"
                            type="password"
                            backgroundColor="rgb(235, 235, 235)"
                            marginBottom="10px"
                            onChange={HandlePassword}
                        />
                        <Button text="Log In" onClick={HandleLogIn} />
                    </div>
                </div>
            </div>
        );
    }
}
export default Admin;
