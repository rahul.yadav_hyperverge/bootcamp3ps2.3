import { useState } from 'react';
import { useHistory } from 'react-router-dom';
import useApi from './useApi';

const useHome = () =>{
    const apiCall = useApi();
    const history = useHistory();
    const [searchMovie , setSearchMovie] = useState();
    const [searchTicket, setSearchTicket] = useState();
    const handleSearchMovie = async () =>{
        const movieString = searchMovie.replace(/ +/g, '-').toLowerCase();
        history.push(`/booking/${movieString}`);
    };
    const handleSearchTicket = async () => {
        history.push(`/ticket/${searchTicket}`);
    }
    return {
        setSearchMovie,
        setSearchTicket,
        handleSearchTicket,
        handleSearchMovie
    };
}
export default useHome;