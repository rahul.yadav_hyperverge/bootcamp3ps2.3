import { useState } from 'react';
import useApi from './useApi';
import { useHistory } from 'react-router-dom';
function getToken() {
	const tokenString = sessionStorage.getItem('token');
	const userToken = JSON.parse(tokenString);
	return userToken;
}
const useAdmin = ({ setTokenState }) => {
    const history = useHistory();
    const apiCall = useApi();
    const [email, setEmail] = useState();
    const [password, setPassword] = useState();
    const [movieName, setMovieName] = useState();
    const [movieDuration, setMovieDuration] = useState();
    const [movieDescription, setMovieDescription] = useState();
    const [theaterLocation, setTheaterLocation] = useState();
    const [moviePrice, setMoviePrice] = useState();
    const HandleEmail = (e) => setEmail(e.target.value);
    const HandlePassword = (e) => setPassword(e.target.value);
    const HandleMovieDescription = (e) => setMovieDescription(e.target.value);
    const HandleMovieDuration = (e) => setMovieDuration(e.target.value);
    const HandleMovieName = (e) => setMovieName(e.target.value);
    const HandleMoviePrice = (e) => setMoviePrice(e.target.value);
    const HandleTheaterLocation = (e) => setTheaterLocation(e.target.value);
    const HandleLogIn = async () => {
        const data = { email, password };
        const { token } = await apiCall({
            data: data,
            path: 'auth/login',
            method: 'POST',
        });
        setToken(token);
    };
    function setToken(userToken) {
        sessionStorage.setItem('token', JSON.stringify(userToken));
        setTokenState(userToken);
    }
    const HandleLogout = async () => {
        sessionStorage.removeItem('token');
        setTokenState('');
        history.push('/');
    };
    const HandleMovieSubmit = async () =>{
        const moviename = movieName.replace(/ +/g, '-').toLowerCase();
        const data = {
            location:theaterLocation,
            price:moviePrice,
            name:moviename,
            description:movieDescription,
            duration:movieDuration
        }
        const token = await getToken();
        const { message } = await apiCall({
            data:data,
            path:'admin/addMovie',
            method:'POST',
            token:token
        })
        alert(message);
    }
    return {
        HandleEmail,
        HandlePassword,
        HandleLogIn,
        HandleLogout,
        HandleMovieDescription,
        HandleMovieDuration,
        HandleMovieName,
        HandleMoviePrice,
        HandleTheaterLocation,
        HandleMovieSubmit
    };
};
export default useAdmin;
