import { useEffect, useState } from 'react';
import useApi from './useApi';
const useTicket = (ticketId) => {
    const apiCall = useApi();
    const [movieName, setMovieName] = useState('');
    const [movieDuration, setMovieDuration] = useState('');
    const [moviePrice, setMoviePrice] = useState('');
    const [movieRow, setMovieRow] = useState('');
    const [movieSeat, setMovieSeat] = useState('');
    const [movieLocation, setMovieLocation] = useState('');
    const [movieUserName, setMovieUserName] = useState('');
    useEffect(async () => {
        const { ticket } = await apiCall({
            path: `query/ticketInfo/${ticketId}`,
            method: 'GET',
        });
        console.log(ticket);
        setMovieName(ticket.movie.name);
        setMovieDuration(ticket.movie.duration);
        setMoviePrice(ticket.theater.price);
        setMovieRow(ticket.seatNumber[0]);
        setMovieLocation(ticket.theater.location);
        setMovieUserName(ticket.user.name)
        if(ticket.seatNumber[2])
            setMovieSeat(`${ticket.seatNumber[1]}${ticket.seatNumber[2]}`);
        else
            setMovieSeat(`${ticket.seatNumber[1]}`);

    }, []);
    return {
        movieName,
        movieDuration,
        moviePrice,
        movieRow,
        movieSeat,
        movieLocation,
        movieUserName
    };
};
export default useTicket;
