import { useEffect, useState } from 'react';
import useApi from './useApi';
import { useHistory } from 'react-router-dom';

const configTheater = (theaters, setTheaterData) => {
    let theater_data = [];
    for (const theater of theaters) {
        theater_data.push({
            location: theater.location,
            id: theater._id,
            price: theater.price,
            tickets: theater.tickets,
        });
    }
    setTheaterData(theater_data);
};
const useBooking = (movieName) => {
    const apiCall = useApi();
    const history = useHistory();
    const [movie, setMovie] = useState('');
    const [date, setDate] = useState();
    const [fare, setFare] = useState(0);
    const [theater, setTheater] = useState('');
    const [time, setTime] = useState('');
    const [theaterData, setTheaterData] = useState([]);
    const [seat, setSeat] = useState();
    const [name, setName] = useState();
    const [phoneNumber, setPhoneNumber] = useState();
    const [seatStatus, setSeatStatus] = useState([
        [
            false,
            false,
            false,
            false,
            false,
            false,
            false,
            false,
            false,
            false,
            false,
            false,
            false,
            false,
            false,
            false,
        ],
        [
            false,
            false,
            false,
            false,
            false,
            false,
            false,
            false,
            false,
            false,
            false,
            false,
            false,
            false,
            false,
            false,
        ],
        [
            false,
            false,
            false,
            false,
            false,
            false,
            false,
            false,
            false,
            false,
            false,
            false,
            false,
            false,
            false,
            false,
        ],
        [
            false,
            false,
            false,
            false,
            false,
            false,
            false,
            false,
            false,
            false,
            false,
            false,
            false,
            false,
            false,
            false,
        ],
        [
            false,
            false,
            false,
            false,
            false,
            false,
            false,
            false,
            false,
            false,
            false,
            false,
            false,
            false,
            false,
            false,
        ],
        [
            false,
            false,
            false,
            false,
            false,
            false,
            false,
            false,
            false,
            false,
            false,
            false,
            false,
            false,
            false,
            false,
        ],
    ]);
    useEffect(async () => {
        const { movie } = await apiCall({
            path: `query/movie/${movieName}`,
            method: 'GET',
        });
        configTheater(movie.theater, setTheaterData);
        setMovie(movie);
    }, []);
    useEffect(() => {
        const targetTheater = theaterData.filter(
            (theaterItem) => theaterItem.id === theater
        );
        if (targetTheater && targetTheater.length > 0) {
            setFare(targetTheater[0].price);
            const alreadyBooked = targetTheater[0].tickets.map(
                (ticketItem) => ticketItem.seatNumber
            );
            console.log('alreadyBooked: ', alreadyBooked);
            if (alreadyBooked && alreadyBooked.length > 0) {
                const updatedSeats = [];
                const rowName = ['A', 'B', 'C', 'D', 'E', 'F'];
                for (let row = 0; row < 6; row++) {
                    const updatedColumn = [];
                    for (let column = 0; column < 18; column++) {
                        if (
                            alreadyBooked.find(
                                (bookedSeatIndex) =>
                                    `${rowName[row]}${column}` ==
                                    bookedSeatIndex
                            )
                        ) {
                            updatedColumn.push(true);
                        } else {
                            updatedColumn.push(false);
                        }
                    }
                    updatedSeats.push(updatedColumn);
                }
                setSeatStatus(updatedSeats);
            }
        }
    }, [theater]);
    const handleCheckout = async () => {
        console.log(theater);
        console.log(seat);
        console.log(date);
        console.log(name);
        console.log(phoneNumber);
        const data = {
            theaterId: theater,
            movieId: movie._id,
            userDetails: {
                name: name,
                phoneNumber: phoneNumber,
            },
            seatNumber: seat,
        };
        const { booked, ticketId } = await apiCall({
            path: `booking/bookTicket`,
            method: 'POST',
            data: data,
        });
        if (booked) {
            history.push(`/ticket/${ticketId}`);
        } else {
            alert('Ticket is not Booked');
        }
    };

    const handleName = (e) => setName(e.target.value);
    const handlePhoneNumber = (e) => setPhoneNumber(e.target.value);
    return {
        movie,
        setDate,
        fare,
        setTheater,
        setTime,
        theater,
        time,
        theaterData,
        handleCheckout,
        setSeat,
        seatStatus,
        handleName,
        handlePhoneNumber,
    };
};
export default useBooking;
