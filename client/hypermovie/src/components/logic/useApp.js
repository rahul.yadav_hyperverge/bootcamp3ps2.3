import { useState, useEffect } from "react";
function getToken() {
	const tokenString = sessionStorage.getItem('token');
	const userToken = JSON.parse(tokenString);
	return userToken;
}
const useApp = () =>{
    const [isAdmin, setIsAdmin] = useState();
    const [tokenState,setTokenState] = useState();
    useEffect(() => {
		const token = getToken();
		setTokenState(token);
		setIsAdmin(tokenState?true:false);
	}, [tokenState]);
    return {
        setTokenState,
        isAdmin
    };
}
export default useApp;