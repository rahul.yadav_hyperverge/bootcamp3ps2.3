import styles from './input.module.css';
function Input(props){
    return (
        <div className={styles.input_box} style={{marginBottom:`${props.marginBottom}`}}>
            <input className={styles.input_tag} type={props.type} style={{backgroundColor:`${props.backgroundColor}`,color:`${props.color}`}} placeholder={props.placeholder} onChange={props.onChange}/>
        </div>
    );
}
export default Input;