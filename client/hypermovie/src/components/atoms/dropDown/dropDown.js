import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';

import './dropDown.css';
function DropDown({value,setTheater,text,data}) {
    const handleChange = (event) => {
        setTheater(event.target.value);
    };
    return (
        <div className="drop_down">
            <FormControl variant="outlined">
                <InputLabel id="demo-simple-select-outlined-label">
                    {text}
                </InputLabel>
                <Select
                    labelId="demo-simple-select-outlined-label"
                    id="demo-simple-select-outlined"
                    value={value}
                    onChange={handleChange}
                    label="Age">
                    {data.map((theater,index)=>(
                        <MenuItem key={index} value={theater.id}>{theater.location}</MenuItem>
                    ))}
                </Select>
            </FormControl>
        </div>
    );
}
export default DropDown;
