import styles from './date.module.css';
function Date({ date, day, setDate }) {
    const handleClick = () => {
        console.log('clicke');
        // TODO: date selection color   
        // for (let i = 26; i <= 29; i++) {
        //     if (i === date){
        //         document.getElementsByClassName(`${styles.date_box}`)[
        //             i - 26
        //         ].style.backgroundColor = 'rgb(0, 204, 255)';
        //     } else {
        //         // document.getElementsByClassName(`${styles.date_box}`)[
        //         //     i - 26
        //         // ].style.backgroundColor = 'transparent';
        //     }
        // }
        document.getElementsByClassName(`${styles.date_box}`)[date - 26]
            .style.backgroundColor = 'rgb(0, 204, 255)';
        setDate(date);
    };
    return (
        <div className={styles.date_box} onClick={handleClick}>
            <div>APR</div>
            <div className={styles.date}>{date}</div>
            <div>{day}</div>
        </div>
    );
}
export default Date;
