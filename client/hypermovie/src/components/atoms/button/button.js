import styles from './button.module.css';
function Button(props){
    return (
        <div className={styles.button_box} onClick={props.onClick} style={{backgroundColor:`${props.backgroundColor}`}}>
            {props.text}
        </div>
    );
}
export default Button;