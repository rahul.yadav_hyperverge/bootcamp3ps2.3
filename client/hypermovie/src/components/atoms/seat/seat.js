import styles from './seat.module.css';
import { MdEventSeat } from 'react-icons/md';
import { useEffect, useState } from 'react';

function Seat({ seatStatus, seatNumber, setSeat }) {
    const [seatColor, setSeatColor] = useState(seatStatus?styles.seat_color_not_available:styles.seat_color);
    const handleSeat = () => {
        setSeat(seatNumber);
        setSeatColor(styles.seat_color_available);
    };
    useEffect(()=>{
        setSeatColor(seatStatus?styles.seat_color_not_available:styles.seat_color);
    },[seatStatus]);
    return (
        <div className={styles.seat_box} onClick={handleSeat}>
            <MdEventSeat className={seatColor} />
        </div>
    );
}
export default Seat;
