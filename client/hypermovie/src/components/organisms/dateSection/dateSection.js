import styles from './dateSection.module.css';
import Date from '../../atoms/date/date';
function DateSeaction({ setDate }) {
    return (
        <div className={styles.date_block}>
            <Date date="26" day="Mon" setDate={setDate} />
            <Date date="27" day="Tue" setDate={setDate} />
            <Date date="28" day="Wed" setDate={setDate} />
            <Date date="29" day="Thu" setDate={setDate} />
        </div>
    );
}
export default DateSeaction;
