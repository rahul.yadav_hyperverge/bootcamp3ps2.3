import styles from './seatingSection.module.css';
import SeatRow from '../seatRow/seatRow';
function Seating({ setSeat, seatStatus }) {
    return (
        <div className={styles.seating_box}>
            <div className={styles.direction_box}>
                <div className={styles.direction_line}></div>
            </div>
            <div className={styles.seats_box}>
                <div className={styles.seat_number_box}>
                    <div className={styles.seat_row}>A</div>
                    <div className={styles.seat_row}>B</div>
                    <div className={styles.seat_row}>C</div>
                    <div className={styles.seat_row}>D</div>
                    <div className={styles.seat_row}>E</div>
                    <div className={styles.seat_row}>F</div>
                </div>
                <div className={styles.seats}>
                    {seatStatus.map((seatNumber,index)=>(
                        <div key={index} className={styles.seat_number}>
                            <SeatRow seatStatus={seatNumber} seatRow={index} setSeat={setSeat}/>
                        </div>
                    ))}
                </div>
                <div className={styles.seat_number_box}>
                    <div className={styles.seat_row}>A</div>
                    <div className={styles.seat_row}>B</div>
                    <div className={styles.seat_row}>C</div>
                    <div className={styles.seat_row}>D</div>
                    <div className={styles.seat_row}>E</div>
                    <div className={styles.seat_row}>F</div>
                </div>
            </div>
        </div>
    );
}
export default Seating;
