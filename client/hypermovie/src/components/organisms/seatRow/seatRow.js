import styles from './seatRow.module.css';
import Seat from '../../atoms/seat/seat';

function SeatRow({ seatStatus, seatRow, setSeat }) {
    let seatnum;
    if (seatRow === 0) seatnum = 'A';
    else if (seatRow === 1) seatnum = 'B';
    else if (seatRow === 2) seatnum = 'C';
    else if (seatRow === 3) seatnum = 'D';
    else if (seatRow === 4) seatnum = 'E';
    else seatnum = 'F';
    return (
        <div className={styles.seat_row}>
            <Seat seatStatus={seatStatus[0]} seatNumber={`${seatnum}1`} setSeat={setSeat}/>
            <Seat seatStatus={seatStatus[1]} seatNumber={`${seatnum}2`} setSeat={setSeat} />
            <Seat seatStatus={seatStatus[2]} seatNumber={`${seatnum}3`} setSeat={setSeat} />
            <Seat seatStatus={seatStatus[3]} seatNumber={`${seatnum}4`} setSeat={setSeat} />
            <Seat seatStatus={seatStatus[4]} seatNumber={`${seatnum}5`} setSeat={setSeat} />
            <Seat seatStatus={seatStatus[5]} seatNumber={`${seatnum}6`} setSeat={setSeat} />
            <Seat seatStatus={seatStatus[6]} seatNumber={`${seatnum}7`} setSeat={setSeat} />
            <Seat seatStatus={seatStatus[7]} seatNumber={`${seatnum}8`} setSeat={setSeat} />
            <Seat seatStatus={seatStatus[8]} seatNumber={`${seatnum}9`} setSeat={setSeat} />
            <div style={{ height: '100%', width: '50px' }} />
            <Seat seatStatus={seatStatus[9]} seatNumber={`${seatnum}10`} setSeat={setSeat} />
            <Seat seatStatus={seatStatus[10]} seatNumber={`${seatnum}11`} setSeat={setSeat} />
            <Seat seatStatus={seatStatus[11]} seatNumber={`${seatnum}12`} setSeat={setSeat} />
            <Seat seatStatus={seatStatus[12]} seatNumber={`${seatnum}13`} setSeat={setSeat} />
            <Seat seatStatus={seatStatus[13]} seatNumber={`${seatnum}14`} setSeat={setSeat} />
            <Seat seatStatus={seatStatus[14]} seatNumber={`${seatnum}15`} setSeat={setSeat} />
            <Seat seatStatus={seatStatus[15]} seatNumber={`${seatnum}16`} setSeat={setSeat} />
            <Seat seatStatus={seatStatus[16]} seatNumber={`${seatnum}17`} setSeat={setSeat} />
            <Seat seatStatus={seatStatus[17]} seatNumber={`${seatnum}18`} setSeat={setSeat} />
        </div>
    );
}
export default SeatRow;
