import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
export { default as HomePage } from './components/pages/home';
export { default as BookingPage } from './components/pages/booking';
export { default as TicketPage } from './components/pages/ticket';
export { default as AdminPage } from './components/pages/admin';
ReactDOM.render(
    <React.StrictMode>
        <App />
    </React.StrictMode>,
    document.getElementById('root')
);
